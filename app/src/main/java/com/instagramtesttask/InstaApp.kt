package com.instagramtesttask

import android.app.Application
import com.instagramtesttask.data.prefs.LoginStorage

/**
 * @author Alex on 20.11.2018.
 */
class InstaApp : Application() {

  override fun onCreate() {
    super.onCreate()
    LoginStorage.initWith(this)
  }

}