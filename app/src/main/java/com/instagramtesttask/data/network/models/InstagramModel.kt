package com.instagramtesttask.data.network.models

import com.instagramtesttask.data.entities.TagItem
import com.instagramtesttask.data.entities.mediaitem.MediaItem
import com.instagramtesttask.data.network.services.InstagramService
import com.instagramtesttask.data.prefs.LoginStorage
import com.instagramtesttask.data.prefs.LoginStorage.LoginManager
import io.reactivex.Single

/**
 * @author Alex on 20.11.2018.
 */

class InstagramModel(
    val service: InstagramService = InstagramService.get(),
    val loginManager: LoginManager = LoginStorage.instance
) {

  companion object {
    private const val DEFAULT_MEDIA_LIMIT = 100
  }

  fun searchTags(query: String): Single<List<TagItem>> {
    return service.searchTags(query, loginManager.token())
        .map { it.list }
  }

  fun getRecentImagesForTag(tagName: String, limit: Int = DEFAULT_MEDIA_LIMIT): Single<List<MediaItem>> {
    return service.getRecentMediaForTag(tagName, limit, loginManager.token())
        .map { it.list.filter(MediaItem::isImage) }
  }
}