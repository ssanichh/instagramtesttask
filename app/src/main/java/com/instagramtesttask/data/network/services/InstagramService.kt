package com.instagramtesttask.data.network.services

import com.instagramtesttask.data.entities.TagItem
import com.instagramtesttask.data.entities.mediaitem.MediaItem
import com.instagramtesttask.data.network.ServiceGenerator
import com.instagramtesttask.data.network.responses.BaseListResponse
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

/**
 * @author Alex on 20.11.2018.
 */
interface InstagramService {

  companion object {
    private const val QUERY = "q"
    private const val ACCESS_TOKEN = "access_token"
    private const val TAG_NAME = "tag_name"
    private const val COUNT = "count"

    fun get(): InstagramService = ServiceGenerator.createService()
  }

  @GET("tags/search")
  fun searchTags(@Query(QUERY) query: String, @Query(ACCESS_TOKEN) token: String): Single<BaseListResponse<TagItem>>

  @GET("tags/{$TAG_NAME}/media/recent")
  fun getRecentMediaForTag(
      @Path(TAG_NAME) tagName: String,
      @Query(COUNT) limit: Int,
      @Query(ACCESS_TOKEN) token: String
  ): Single<BaseListResponse<MediaItem>>
}