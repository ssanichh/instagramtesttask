package com.instagramtesttask.data.network

import android.util.Log
import com.ihsanbal.logging.Level
import com.ihsanbal.logging.LoggingInterceptor
import com.instagramtesttask.utils.extensions.isLogsEnabled
import io.reactivex.schedulers.Schedulers
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

/**
 * @author Alex on 20.11.2018.
 */
object ServiceGenerator {

  private const val BASE_URL = "https://api.instagram.com/v1/"
  private var retrofit: Retrofit? = null

  inline fun <reified S> createService(): S {
    return getRetrofit().create(S::class.java)
  }

  fun getRetrofit(): Retrofit {
    if (retrofit == null) {
      retrofit = Retrofit.Builder()
          .baseUrl(BASE_URL)
          .addConverterFactory(GsonConverterFactory.create())
          .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
          .client(createOkHttp())
          .build()
    }

    return retrofit!!
  }

  private fun createOkHttp(): OkHttpClient {
    return OkHttpClient.Builder()
        .addInterceptor(createLoggingInterceptor())
        .build()
  }

  private fun createLoggingInterceptor(): LoggingInterceptor {
    val logLevel = if (isLogsEnabled()) Level.BASIC else Level.NONE
    return LoggingInterceptor.Builder()
        .loggable(isLogsEnabled())
        .setLevel(logLevel)
        .log(Log.INFO)
        .request("")
        .response("")
        .enableAndroidStudio_v3_LogsHack(true)
        .build()
  }
}