package com.instagramtesttask.data.network.responses

import com.google.gson.annotations.SerializedName

/**
 * @author Alex on 20.11.2018.
 */
class BaseListResponse<out S> {
  @SerializedName("data")
  private val _list: List<S>? = emptyList()

  val list: List<S>
    get() = _list ?: emptyList()
}