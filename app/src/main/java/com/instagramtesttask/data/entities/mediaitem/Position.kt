package com.instagramtesttask.data.entities.mediaitem

data class Position(
	val X: Double? = null,
	val Y: Double? = null
)
