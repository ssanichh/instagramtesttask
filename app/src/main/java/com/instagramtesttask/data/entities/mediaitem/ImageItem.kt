package com.instagramtesttask.data.entities.mediaitem

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

data class ImageItem(
    @SerializedName("width") val width: Int = 0,
    @SerializedName("height") val height: Int = 0,
    @SerializedName("url") val url: String = ""
) : Parcelable {
  constructor(source: Parcel) : this(
      source.readInt(),
      source.readInt(),
      source.readString()
  )

  override fun describeContents() = 0

  override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
    writeInt(width)
    writeInt(height)
    writeString(url)
  }

  companion object {
    @JvmField
    val CREATOR: Parcelable.Creator<ImageItem> = object : Parcelable.Creator<ImageItem> {
      override fun createFromParcel(source: Parcel): ImageItem = ImageItem(source)
      override fun newArray(size: Int): Array<ImageItem?> = arrayOfNulls(size)
    }
  }
}
