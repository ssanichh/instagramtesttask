package com.instagramtesttask.data.entities.mediaitem

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

data class MediaItem(
    @SerializedName("id") val id: String,
    @SerializedName("user") val user: User,
    @SerializedName("images") val images: Images,
    @SerializedName("type") val type: String,
    @SerializedName("link") val link: String,
    @SerializedName("tags") val tags: List<String>,
    @SerializedName("created_time") val createdTime: String,
    @SerializedName("caption") val caption: Caption? = null
) : Parcelable {

  companion object {
    const val TYPE_IMAGE = "image"
    const val TYPE_VIDEO = "video"

    @JvmField
    val CREATOR: Parcelable.Creator<MediaItem> = object : Parcelable.Creator<MediaItem> {
      override fun createFromParcel(source: Parcel): MediaItem = MediaItem(source)
      override fun newArray(size: Int): Array<MediaItem?> = arrayOfNulls(size)
    }
  }

  constructor(source: Parcel) : this(
      source.readString(),
      source.readParcelable<User>(User::class.java.classLoader),
      source.readParcelable<Images>(Images::class.java.classLoader),
      source.readString(),
      source.readString(),
      source.createStringArrayList(),
      source.readString(),
      source.readParcelable<Caption?>(Caption::class.java.classLoader)
  )

  override fun describeContents() = 0

  override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
    writeString(id)
    writeParcelable(user, 0)
    writeParcelable(images, 0)
    writeString(type)
    writeString(link)
    writeStringList(tags)
    writeString(createdTime)
    writeParcelable(caption, 0)
  }

  fun isImage(): Boolean = type == TYPE_IMAGE
}