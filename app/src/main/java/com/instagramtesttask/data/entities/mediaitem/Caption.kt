package com.instagramtesttask.data.entities.mediaitem

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

data class Caption(
    @SerializedName("id") val id: String = "",
    @SerializedName("created_time") val createdTime: String = "",
    @SerializedName("from") val from: User = User(),
    @SerializedName("text") val text: String = ""
) : Parcelable {
  constructor(source: Parcel) : this(
      source.readString(),
      source.readString(),
      source.readParcelable<User>(User::class.java.classLoader),
      source.readString()
  )

  override fun describeContents() = 0

  override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
    writeString(id)
    writeString(createdTime)
    writeParcelable(from, 0)
    writeString(text)
  }

  companion object {
    @JvmField
    val CREATOR: Parcelable.Creator<Caption> = object : Parcelable.Creator<Caption> {
      override fun createFromParcel(source: Parcel): Caption = Caption(source)
      override fun newArray(size: Int): Array<Caption?> = arrayOfNulls(size)
    }
  }
}
