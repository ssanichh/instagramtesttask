package com.instagramtesttask.data.entities.mediaitem

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

data class User(
    @SerializedName("id") val id: String? = null,
    @SerializedName("full_name") val fullName: String? = null,
    @SerializedName("username") val username: String = "",
    @SerializedName("profile_picture") val profilePicture: String? = null
) : Parcelable {
  constructor(source: Parcel) : this(
      source.readString(),
      source.readString(),
      source.readString(),
      source.readString()
  )

  override fun describeContents() = 0

  override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
    writeString(id)
    writeString(fullName)
    writeString(username)
    writeString(profilePicture)
  }

  fun getAvailableUserName(): String = fullName?.takeIf { it.isNotBlank() } ?: username

  companion object {
    @JvmField
    val CREATOR: Parcelable.Creator<User> = object : Parcelable.Creator<User> {
      override fun createFromParcel(source: Parcel): User = User(source)
      override fun newArray(size: Int): Array<User?> = arrayOfNulls(size)
    }
  }
}
