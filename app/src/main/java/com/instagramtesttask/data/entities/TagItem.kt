package com.instagramtesttask.data.entities

import com.google.gson.annotations.SerializedName

/**
 * @author Alex on 20.11.2018.
 */
data class TagItem(
    @SerializedName("name") val name: String,
    @SerializedName("media_count") val mediaCount: Long
)