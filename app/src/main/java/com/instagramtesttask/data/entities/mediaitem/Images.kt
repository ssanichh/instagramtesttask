package com.instagramtesttask.data.entities.mediaitem

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

data class Images(
    @SerializedName("thumbnail") val thumbnail: ImageItem? = null,
    @SerializedName("low_resolution") val lowResolution: ImageItem? = null,
    @SerializedName("standard_resolution") val standardResolution: ImageItem? = null
) : Parcelable {
  fun getBestImageUrl(): String? {
    val urls = listOf(standardResolution?.url, lowResolution?.url, thumbnail?.url)
    return urls.firstOrNull { it != null }
  }

  constructor(source: Parcel) : this(
      source.readParcelable<ImageItem>(ImageItem::class.java.classLoader),
      source.readParcelable<ImageItem>(ImageItem::class.java.classLoader),
      source.readParcelable<ImageItem>(ImageItem::class.java.classLoader)
  )

  override fun describeContents() = 0

  override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
    writeParcelable(thumbnail, 0)
    writeParcelable(lowResolution, 0)
    writeParcelable(standardResolution, 0)
  }

  companion object {
    @JvmField
    val CREATOR: Parcelable.Creator<Images> = object : Parcelable.Creator<Images> {
      override fun createFromParcel(source: Parcel): Images = Images(source)
      override fun newArray(size: Int): Array<Images?> = arrayOfNulls(size)
    }
  }
}
