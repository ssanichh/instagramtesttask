package com.instagramtesttask.data.prefs

import android.content.Context
import android.content.SharedPreferences

/**
 * @author Alex on 20.11.2018.
 */
open class BasePreferences(val context: Context) {
  val preferences: SharedPreferences by lazy { context.getSharedPreferences("insta_preferences", Context.MODE_PRIVATE); }
  fun prefString() = PrefStringDelegate(preferences)
  fun prefLong() = PrefLongDelegate(preferences)
  fun prefInt(defVal: Int = -1) = PrefIntDelegate(preferences, defVal)
  fun prefBoolean() = PrefBooleanDelegate(preferences)
}