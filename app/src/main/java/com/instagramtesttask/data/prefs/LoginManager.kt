package com.instagramtesttask.data.prefs

import android.content.Context
import com.instagramtesttask.data.prefs.LoginStorage.LoginManager

/**
 * @author Alex on 20.11.2018.
 */
object LoginStorage {
  lateinit var instance: LoginManager

  fun initWith(context: Context) {
    instance = LoginPreferences(context)
  }

  interface LoginManager {
    fun saveToken(token: String)
    fun token(): String
    fun isAuthorized(): Boolean
  }
}

private class LoginPreferences(context: Context): BasePreferences(context), LoginManager {
  private var token by prefString()

  override fun saveToken(token: String) {
    this.token = token
  }

  override fun token(): String = token

  override fun isAuthorized(): Boolean = token.isNotBlank()
}