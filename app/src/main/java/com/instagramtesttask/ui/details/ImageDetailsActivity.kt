package com.instagramtesttask.ui.details

import android.app.ActivityOptions
import android.content.Intent
import android.os.Bundle
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.instagramtesttask.R
import com.instagramtesttask.data.entities.mediaitem.MediaItem
import com.instagramtesttask.data.entities.mediaitem.User
import com.instagramtesttask.utils.GlideImageLoadListener
import jp.wasabeef.glide.transformations.CropCircleTransformation
import kotlinx.android.synthetic.main.activity_image_details.imgAvatar
import kotlinx.android.synthetic.main.activity_image_details.imgImage
import kotlinx.android.synthetic.main.activity_image_details.tvName

class ImageDetailsActivity : AppCompatActivity() {

  companion object {
    private const val EXTRA_IMAGE = "key_image"

    @JvmStatic
    fun launch(context: AppCompatActivity, image: MediaItem, sharedView: ImageView) {
      val intent = Intent(context, ImageDetailsActivity::class.java)
          .putExtra(EXTRA_IMAGE, image)
      val options = ActivityOptions.makeSceneTransitionAnimation(context, sharedView, image.id)
      context.startActivity(intent, options.toBundle())
    }
  }

  private val extraImage: MediaItem? by lazy { intent?.getParcelableExtra<MediaItem>(EXTRA_IMAGE) }

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_image_details)
    if (extraImage == null) finish()
    postponeEnterTransition()
    imgImage.transitionName = extraImage!!.id
    setupUserData(extraImage!!.user)
    loadImage(extraImage!!.images.getBestImageUrl())
  }

  private fun loadImage(imageUrl: String?) {
    Glide.with(this)
        .load(imageUrl)
        .error(R.drawable.bg_image_placeholder)
        .listener(object : GlideImageLoadListener() {
          override fun onImageLoaded() {
            startPostponedEnterTransition()
          }
        })
        .into(imgImage)
  }

  private fun setupUserData(user: User) {
    tvName.text = user.getAvailableUserName()
    Glide.with(this)
        .load(user.profilePicture)
        .placeholder(R.drawable.ic_avatar_holder)
        .error(R.drawable.ic_avatar_holder)
        .bitmapTransform(CropCircleTransformation(this))
        .into(imgAvatar)
  }
}
