package com.instagramtesttask.ui.mainscreen

import com.instagramtesttask.data.entities.TagItem
import com.instagramtesttask.data.entities.mediaitem.MediaItem
import com.instagramtesttask.ui.base.BaseMVPView
import com.instagramtesttask.ui.base.IBasePresenter
import io.reactivex.Observable

/**
 * @author Alex on 20.11.2018.
 */
interface MainScreenContract {

  interface View : BaseMVPView {
    fun launchAuthScreen()
    fun showAuthOverlay(shouldShow: Boolean)
    fun getSearchObservable(): Observable<String>
    fun updateTagsList(tags: List<TagItem>)
    fun updateImagesList(images: List<MediaItem>)
    fun showImagesEmptyState()
  }

  interface Presenter : IBasePresenter {
    fun onAuthSucceed(token: String)
    fun onAuthError()
    fun onAuthorizeClicked()
    fun onTagChoosed(tag: String)
  }

  object ProgressCodes {
    const val SEARCH_PROGRESS = 1
  }
}