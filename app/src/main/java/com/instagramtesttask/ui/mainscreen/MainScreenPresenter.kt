package com.instagramtesttask.ui.mainscreen

import com.instagramtesttask.data.entities.TagItem
import com.instagramtesttask.data.network.models.InstagramModel
import com.instagramtesttask.data.prefs.LoginStorage
import com.instagramtesttask.ui.base.BasePresenter
import com.instagramtesttask.ui.mainscreen.MainScreenContract.Presenter
import com.instagramtesttask.ui.mainscreen.MainScreenContract.ProgressCodes
import com.instagramtesttask.ui.mainscreen.MainScreenContract.View
import com.instagramtesttask.utils.extensions.addToCompositeDisposable
import com.instagramtesttask.utils.extensions.io
import com.instagramtesttask.utils.extensions.mainThread
import io.reactivex.Single
import java.util.concurrent.TimeUnit

/**
 * @author Alex on 20.11.2018.
 */
class MainScreenPresenter(override val view: View) : BasePresenter(view), Presenter {

  companion object {
    private const val SEARCH_DEBOUNCE = 400L
  }

  private val loginManager = LoginStorage.instance
  private val instagramModel = InstagramModel()

  override fun onCreate() {
    super.onCreate()
    if (!loginManager.isAuthorized()) {
      view.launchAuthScreen()
    }
  }

  override fun onStart() {
    super.onStart()
    subscribeToSearchChanges()
  }

  override fun onAuthSucceed(token: String) {
    loginManager.saveToken(token)
    view.showAuthOverlay(false)
  }

  override fun onAuthError() {
    view.showAuthOverlay(true)
  }

  override fun onAuthorizeClicked() {
    view.launchAuthScreen()
  }

  override fun onTagChoosed(tag: String) {
    instagramModel.getRecentImagesForTag(tag)
        .networkRequestTransformer()
        .subscribe({
          if (it.isEmpty()) view.showImagesEmptyState()
          else view.updateImagesList(it)
        }, ::onError)
        .addToCompositeDisposable(disposables)
  }

  private fun subscribeToSearchChanges() {
    view.getSearchObservable()
        .mainThread()
        .doOnNext { view.showProgress(ProgressCodes.SEARCH_PROGRESS) }
        .debounce(SEARCH_DEBOUNCE, TimeUnit.MILLISECONDS)
        .io()
        .switchMapSingle(::networkRequestSingle)
        .mainThread()
        .doOnNext { view.hideProgress(ProgressCodes.SEARCH_PROGRESS) }
        .subscribe(view::updateTagsList, ::onError)
        .addToCompositeDisposable(disposables)
  }

  private fun networkRequestSingle(it: String): Single<List<TagItem>> {
    return if (it.isEmpty()) Single.just(emptyList())
    else {
      instagramModel.searchTags(it)
          .onErrorResumeNext {
            it.printStackTrace()
            Single.just(emptyList())
          }
    }
  }

}