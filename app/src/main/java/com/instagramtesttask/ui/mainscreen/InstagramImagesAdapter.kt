package com.instagramtesttask.ui.mainscreen

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.instagramtesttask.R
import com.instagramtesttask.data.entities.mediaitem.MediaItem
import com.instagramtesttask.ui.mainscreen.InstagramImagesAdapter.ViewHolder
import jp.wasabeef.glide.transformations.CropCircleTransformation
import kotlinx.android.synthetic.main.item_image.view.cvImageRoot
import kotlinx.android.synthetic.main.item_image.view.imgAvatar
import kotlinx.android.synthetic.main.item_image.view.imgImage
import kotlinx.android.synthetic.main.item_image.view.tvName

/**
 * @author Alex on 21.11.2018.
 */
class InstagramImagesAdapter(
    private val items: MutableList<MediaItem> = mutableListOf(),
    private val clickListener: (MediaItem, ImageView) -> Unit
) : RecyclerView.Adapter<ViewHolder>() {

  override fun getItemCount(): Int = items.size

  override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
    val itemView = LayoutInflater.from(parent.context).inflate(R.layout.item_image, parent, false)
    return ViewHolder(itemView)
  }

  override fun onBindViewHolder(holder: ViewHolder, position: Int) {
    holder.bind(items[position], clickListener)
  }

  fun updateItems(items: List<MediaItem>) {
    dispatchDiff(items)
    this.items.clear()
    this.items.addAll(items)
  }

  fun clear() {
    dispatchDiff(emptyList())
    this.items.clear()
  }

  private fun dispatchDiff(newList: List<MediaItem>) {
    val diffResult = DiffUtil.calculateDiff(DiffCallback(this.items, newList))
    diffResult.dispatchUpdatesTo(this)
  }

  class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    fun bind(item: MediaItem, clickListener: (MediaItem, ImageView) -> Unit) {
      itemView.apply {
        imgImage.transitionName = item.id
        cvImageRoot.setOnClickListener { clickListener.invoke(item, imgImage) }
        tvName.text = item.user.getAvailableUserName()
        loadUserAvatar(imgAvatar, item.user.profilePicture)
        loadImage(imgImage, item.images.getBestImageUrl())
      }
    }

    private fun loadUserAvatar(imgAvatar: ImageView, avatarUrl: String?) {
      val context = imgAvatar.context.applicationContext
      Glide.clear(imgAvatar)
      Glide.with(context)
          .load(avatarUrl)
          .bitmapTransform(CropCircleTransformation(context))
          .placeholder(R.drawable.ic_avatar_holder)
          .error(R.drawable.ic_avatar_holder)
          .into(imgAvatar)
    }

    private fun loadImage(imgImage: ImageView, imageUrl: String?) {
      val context = imgImage.context.applicationContext
      Glide.clear(imgImage)
      Glide.with(context)
          .load(imageUrl)
          .placeholder(R.drawable.bg_image_placeholder)
          .error(R.drawable.bg_image_placeholder)
          .centerCrop()
          .into(imgImage)
    }
  }

  class DiffCallback(private val oldList: List<MediaItem>, private val newList: List<MediaItem>) : DiffUtil.Callback() {

    override fun getOldListSize(): Int = oldList.size
    override fun getNewListSize(): Int = newList.size

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
      return oldList[oldItemPosition].id == newList[newItemPosition].id
    }

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
      val oldUrl = oldList[oldItemPosition].images.getBestImageUrl()
      val newUrl = newList[newItemPosition].images.getBestImageUrl()
      return oldUrl == newUrl
    }
  }

}