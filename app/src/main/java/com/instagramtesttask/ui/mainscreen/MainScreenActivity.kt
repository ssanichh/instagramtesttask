package com.instagramtesttask.ui.mainscreen

import android.app.Activity
import android.app.SearchManager
import android.content.Intent
import android.database.Cursor
import android.database.MatrixCursor
import android.os.Bundle
import android.provider.BaseColumns
import android.widget.ImageView
import androidx.appcompat.widget.SearchView.OnSuggestionListener
import androidx.cursoradapter.widget.CursorAdapter
import androidx.cursoradapter.widget.SimpleCursorAdapter
import androidx.recyclerview.widget.GridLayoutManager
import com.instagramtesttask.R
import com.instagramtesttask.data.entities.TagItem
import com.instagramtesttask.data.entities.mediaitem.MediaItem
import com.instagramtesttask.ui.auth.InstagramAuthActivity
import com.instagramtesttask.ui.base.BaseActivity
import com.instagramtesttask.ui.details.ImageDetailsActivity
import com.instagramtesttask.ui.mainscreen.MainScreenContract.Presenter
import com.instagramtesttask.ui.mainscreen.MainScreenContract.ProgressCodes
import com.instagramtesttask.ui.mainscreen.MainScreenContract.View
import com.instagramtesttask.utils.extensions.hideAnim
import com.instagramtesttask.utils.extensions.invisibleAnim
import com.instagramtesttask.utils.extensions.showAnim
import com.instagramtesttask.utils.recycler.ItemOffsetDecoration
import com.jakewharton.rxbinding3.appcompat.queryTextChanges
import io.reactivex.Observable
import jp.wasabeef.recyclerview.animators.SlideInUpAnimator
import kotlinx.android.synthetic.main.activity_main.btnAuthorize
import kotlinx.android.synthetic.main.activity_main.clAuthOverlay
import kotlinx.android.synthetic.main.activity_main.flEmptyView
import kotlinx.android.synthetic.main.activity_main.rvImages
import kotlinx.android.synthetic.main.activity_main.searchProgress
import kotlinx.android.synthetic.main.activity_main.searchView
import kotlinx.android.synthetic.main.activity_main.tvEmptyState
import java.text.DecimalFormat
import java.text.DecimalFormatSymbols

/**
 * @author Alex on 20.11.2018.
 */
class MainScreenActivity : BaseActivity(), View {

  companion object {
    private const val GRID_SPAN_COUNT = 2
  }

  override val presenter: Presenter by lazy { MainScreenPresenter(this) }
  private val imagesAdapter by lazy { InstagramImagesAdapter(clickListener = ::onImageClicked) }
  private val tagsAdapter: CursorAdapter by lazy {
    SimpleCursorAdapter(
        this,
        R.layout.item_tag,
        null,
        arrayOf(SearchManager.SUGGEST_COLUMN_TEXT_1, SearchManager.SUGGEST_COLUMN_TEXT_2),
        intArrayOf(R.id.tvTagTitle, R.id.tvMediaCount),
        0
    )
  }

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_main)
    setupUI()
  }

  override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
    super.onActivityResult(requestCode, resultCode, data)
    if (requestCode == InstagramAuthActivity.REQUEST_CODE) {
      val token = InstagramAuthActivity.fetchToken(data)
      if (resultCode == Activity.RESULT_OK && token != null) {
        presenter.onAuthSucceed(token)
      } else {
        presenter.onAuthError()
      }
    }
  }

  override fun showProgress(requestId: Int?) {
    when (requestId) {
      ProgressCodes.SEARCH_PROGRESS -> searchProgress.showAnim()
      else -> super.showProgress(requestId)
    }
  }

  override fun hideProgress(requestId: Int?) {
    when (requestId) {
      ProgressCodes.SEARCH_PROGRESS -> searchProgress.invisibleAnim()
      else -> super.hideProgress(requestId)
    }
  }

  override fun launchAuthScreen() {
    InstagramAuthActivity.launch4Result(this)
  }

  override fun showAuthOverlay(shouldShow: Boolean) {
    if (shouldShow) {
      clAuthOverlay.showAnim()
    } else {
      clAuthOverlay.hideAnim()
    }
  }

  override fun getSearchObservable(): Observable<String> = searchView.queryTextChanges()
      .skipInitialValue()
      .map(CharSequence::toString)

  override fun updateTagsList(tags: List<TagItem>) {
    val cursor = prepareNewTagsCursor(tags)
    tagsAdapter.swapCursor(cursor)
  }

  override fun updateImagesList(images: List<MediaItem>) {
    flEmptyView.hideAnim()
    imagesAdapter.updateItems(images)
  }

  override fun showImagesEmptyState() {
    tvEmptyState.setText(R.string.empty_state)
    flEmptyView.showAnim()
    imagesAdapter.clear()
  }

  private fun setupUI() {
    initRecycler()
    initSearchView()
    setupClickListeners()
  }

  private fun initRecycler() {
    rvImages.layoutManager = GridLayoutManager(this, GRID_SPAN_COUNT)
    rvImages.addItemDecoration(ItemOffsetDecoration(this, R.dimen.recycler_item_spacing))
    rvImages.itemAnimator = SlideInUpAnimator()
    rvImages.adapter = imagesAdapter
  }

  private fun initSearchView() {
    searchView.suggestionsAdapter = tagsAdapter
    searchView.setOnSuggestionListener(prepareSuggestionListener())
  }

  private fun prepareSuggestionListener(): OnSuggestionListener {
    return object : OnSuggestionListener {
      override fun onSuggestionSelect(position: Int): Boolean {
        getClickedItemFromTagsAdapter(position)?.let {
          presenter.onTagChoosed(it)
        }
        return true
      }

      override fun onSuggestionClick(position: Int): Boolean {
        getClickedItemFromTagsAdapter(position)?.let {
          presenter.onTagChoosed(it)
        }
        return true
      }
    }
  }

  private fun setupClickListeners() {
    btnAuthorize.setOnClickListener { presenter.onAuthorizeClicked() }
  }

  private fun onImageClicked(item: MediaItem, imageView: ImageView) {
    ImageDetailsActivity.launch(this, item, imageView)
  }

  private fun prepareNewTagsCursor(tags: List<TagItem>): Cursor {
    val columns = arrayOf(BaseColumns._ID,
        SearchManager.SUGGEST_COLUMN_TEXT_1,
        SearchManager.SUGGEST_COLUMN_TEXT_2,
        SearchManager.SUGGEST_COLUMN_INTENT_DATA)

    val cursor = MatrixCursor(columns)
    tags.forEachIndexed { index, item ->
      val quantity = item.mediaCount.toInt()
      val quantityText = prepareQuantityString(quantity)
      val mediaCountText = resources.getQuantityString(R.plurals.posts_count, quantity, quantityText)
      cursor.addRow(arrayOf(index.toString(), item.name, mediaCountText, item.name))
    }
    return cursor
  }

  private fun prepareQuantityString(quantity: Int): String {
    val df = DecimalFormat()
    df.decimalFormatSymbols = DecimalFormatSymbols().apply { groupingSeparator = ' ' }
    df.groupingSize = 3
    return df.format(quantity)
  }

  private fun getClickedItemFromTagsAdapter(position: Int): String? {
    val cursor = tagsAdapter.getItem(position) as? MatrixCursor ?: return null
    val columnIndex = cursor.getColumnIndex(SearchManager.SUGGEST_COLUMN_TEXT_1)
    return cursor.getString(columnIndex)
  }
}