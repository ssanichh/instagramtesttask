package com.instagramtesttask.ui.auth

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.webkit.CookieManager
import android.webkit.WebResourceRequest
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.appcompat.app.AppCompatActivity
import com.instagramtesttask.R
import com.instagramtesttask.utils.extensions.hide
import com.instagramtesttask.utils.extensions.hideAnim
import com.instagramtesttask.utils.extensions.show
import kotlinx.android.synthetic.main.activity_instagram_auth.progressBar
import kotlinx.android.synthetic.main.activity_instagram_auth.webView

/**
 * @author Alex on 20.11.2018.
 */
class InstagramAuthActivity : AppCompatActivity() {

  companion object {
    const val REQUEST_CODE = 1111
    private const val TOKEN = "access_token"

    @JvmStatic
    fun launch4Result(activity: AppCompatActivity) {
      val intent = Intent(activity, InstagramAuthActivity::class.java)
      activity.startActivityForResult(intent, REQUEST_CODE)
    }

    fun fetchToken(intent: Intent?): String? = intent?.getStringExtra(TOKEN) ?: ""
  }

  private val apiKey by lazy { getString(R.string.instagram_api_key) }
  private val redirectUrl by lazy { getString(R.string.instagram_redirect_url) }
  private val authUrl by lazy { getString(R.string.instagram_authorization_url, apiKey, redirectUrl) }

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_instagram_auth)
    initWebView()
    progressBar?.show()
    webView.loadUrl(authUrl)
  }

  private fun initWebView() {
    CookieManager.getInstance().apply {
      removeAllCookies(null)
      flush()
    }
    webView.apply {
      settings.javaScriptEnabled = true
      settings.domStorageEnabled = true
      clearCache(true)
      clearHistory()
      webViewClient = this@InstagramAuthActivity.getWebViewClient()
    }
  }

  private fun getWebViewClient(): WebViewClient = object : WebViewClient() {

    override fun onPageFinished(view: WebView?, url: String?) {
      super.onPageFinished(view, url)
      progressBar.hide()
    }

    override fun shouldOverrideUrlLoading(view: WebView, url: String): Boolean {
      return onShouldOverrideUrlLoading(view, url)
    }

    override fun shouldOverrideUrlLoading(view: WebView, request: WebResourceRequest): Boolean {
      return onShouldOverrideUrlLoading(view, request.url.toString())
    }
  }

  private fun onShouldOverrideUrlLoading(view: WebView, url: String): Boolean {
    if (url.startsWith(redirectUrl)) {
      onRedirectUrlLoaded(Uri.parse(url))
    } else {
      view.loadUrl(url)
    }
    return true
  }

  private fun onRedirectUrlLoaded(uri: Uri) {
    val token: String? = uri.fragment?.substringAfter("access_token=", "")
        ?.takeIf { it.isNotBlank() }
    webView.hideAnim()
    if (token != null) {
      val resultIntent = Intent().putExtra(TOKEN, token)
      setResult(RESULT_OK, resultIntent)
    } else {
      setResult(Activity.RESULT_CANCELED)
    }
    finish()
  }

}