package com.instagramtesttask.ui.base

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.instagramtesttask.R
import com.instagramtesttask.utils.extensions.hideAnim
import com.instagramtesttask.utils.extensions.message
import com.instagramtesttask.utils.extensions.okButton
import com.instagramtesttask.utils.extensions.showAlert
import com.instagramtesttask.utils.extensions.showAnim

/**
 * @author Alex on 20.11.2018.
 */
abstract class BaseActivity : AppCompatActivity(), BaseMVPView {

  abstract val presenter: IBasePresenter

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    presenter.onCreate()
  }

  override fun onStart() {
    super.onStart()
    presenter.onStart()
  }

  override fun onResume() {
    super.onResume()
    presenter.onResume()
  }

  override fun onPause() {
    presenter.onPause()
    super.onPause()
  }

  override fun onStop() {
    presenter.onStop()
    super.onStop()
  }

  override fun onRequestBegin(requestId: Int?) {
    showProgress(requestId)
  }

  override fun onRequestTerminate(requestId: Int?) {
    hideProgress(requestId)
  }

  override fun showProgress(requestId: Int?) {
    findViewById<View>(R.id.progress)?.showAnim()
  }

  override fun hideProgress(requestId: Int?) {
    findViewById<View>(R.id.progress)?.hideAnim()
  }

  override fun showErrorMessage(msg: CharSequence) {
    runOnUiThread {
      showAlert {
        setTitle(R.string.error_title)
        message(msg.toString())
        okButton { }
        setCancelable(true)
      }
    }
  }

  override fun showSimpleDialog(msg: CharSequence) {
    showAlert {
      message(msg.toString())
      okButton { }
      setCancelable(true)
    }
  }

  protected fun showErrorMessage(resId: Int) {
    val str = resources.getString(resId)
    showErrorMessage(str)
  }

}