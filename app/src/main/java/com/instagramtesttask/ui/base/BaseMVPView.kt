package com.instagramtesttask.ui.base

/**
 * @author Alex on 20.11.2018.
 */
interface BaseMVPView {
  fun onRequestBegin(requestId: Int? = null)
  fun onRequestTerminate(requestId: Int? = null)
  fun showProgress(requestId: Int? = null)
  fun hideProgress(requestId: Int? = null)
  fun showErrorMessage(msg: CharSequence)
  fun showSimpleDialog(msg: CharSequence)
}