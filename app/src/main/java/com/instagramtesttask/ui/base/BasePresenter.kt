package com.instagramtesttask.ui.base

import com.instagramtesttask.utils.extensions.isLogsEnabled
import com.instagramtesttask.utils.extensions.mainThread
import io.reactivex.FlowableTransformer
import io.reactivex.ObservableTransformer
import io.reactivex.SingleTransformer
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

/**
 * @author Alex on 20.11.2018.
 */
abstract class BasePresenter(open val view: BaseMVPView) : IBasePresenter {

  protected val disposables = CompositeDisposable()

  override fun onCreate() {

  }

  override fun onStart() {

  }

  override fun onResume() {

  }

  override fun onPause() {

  }

  override fun onStop() {
    disposables.clear()
  }


  override fun <T> createRequestTransformer(requestId: Int?) = FlowableTransformer<T, T> { flowable ->
    flowable
        .subscribeOn(Schedulers.io())
        .mainThread()
        .doOnSubscribe { view.onRequestBegin(requestId) }
        .doFinally { view.onRequestTerminate(requestId) }
  }

  override fun <T> createRequestObservableTransformer(requestId: Int?) =
      ObservableTransformer<T, T> { observable ->
        observable
            .subscribeOn(Schedulers.io())
            .mainThread()
            .doOnSubscribe { view.onRequestBegin(requestId) }
            .doOnTerminate { view.onRequestTerminate(requestId) }
      }

  override fun <T> createRequestTransformerSingle(requestId: Int?) =
      SingleTransformer<T, T> { single ->
        single
            .subscribeOn(Schedulers.io())
            .mainThread()
            .doOnSubscribe { view.onRequestBegin(requestId) }
            .doFinally { view.onRequestTerminate(requestId) }
      }

  //override for custom error handling
  override fun onError(throwable: Throwable) {
    if (isLogsEnabled()) {
      throwable.printStackTrace()
    }
  }

}