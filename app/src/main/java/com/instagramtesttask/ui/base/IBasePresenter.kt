package com.instagramtesttask.ui.base

import io.reactivex.Flowable
import io.reactivex.FlowableTransformer
import io.reactivex.Observable
import io.reactivex.ObservableTransformer
import io.reactivex.Single
import io.reactivex.SingleTransformer

/**
 * @author Alex on 20.11.2018.
 */
interface IBasePresenter {
  fun onCreate()
  fun onStart()
  fun onResume()
  fun onPause()
  fun onStop()
  fun <T> createRequestTransformer(requestId: Int?): FlowableTransformer<T, T>
  fun <T> createRequestObservableTransformer(requestId: Int?): ObservableTransformer<T, T>
  fun <T> createRequestTransformerSingle(requestId: Int?): SingleTransformer<T, T>
  fun onError(throwable: Throwable)

  fun <T> Flowable<T>.networkRequestTransformer(requestId: Int? = null) = compose(createRequestTransformer<T>(requestId))
  fun <T> Observable<T>.networkRequestTransformer(requestId: Int? = null) = compose(createRequestObservableTransformer<T>(requestId))
  fun <T> Single<T>.networkRequestTransformer(requestId: Int? = null) = compose(createRequestTransformerSingle<T>(requestId))

}