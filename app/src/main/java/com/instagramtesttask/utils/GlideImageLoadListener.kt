package com.instagramtesttask.utils

import com.bumptech.glide.load.resource.drawable.GlideDrawable
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.instagramtesttask.utils.extensions.isLogsEnabled

/**
 * @author Alex on 21.11.2018.
 */
abstract class GlideImageLoadListener : RequestListener<String, GlideDrawable> {

  override fun onResourceReady(resource: GlideDrawable?, model: String?, target: Target<GlideDrawable>?, isFromMemoryCache: Boolean, isFirstResource: Boolean): Boolean {
    onImageLoaded()
    return false
  }

  override fun onException(e: Exception?, model: String?, target: Target<GlideDrawable>?, isFirstResource: Boolean): Boolean {
    if (isLogsEnabled()) e?.printStackTrace()
    onImageLoaded()
    return false
  }

  abstract fun onImageLoaded()

}