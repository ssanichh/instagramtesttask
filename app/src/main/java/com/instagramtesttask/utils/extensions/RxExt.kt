package com.instagramtesttask.utils.extensions

import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

/**
 * @author Alex on 20.11.2018.
 */
fun <T> Observable<T>.mainThread() = compose { observeOn(AndroidSchedulers.mainThread()) }
fun <T> Observable<T>.io() = compose { observeOn(Schedulers.io()) }
fun <T> Observable<T>.computation() = compose { observeOn(Schedulers.computation()) }

fun Completable.mainThread() = compose { observeOn(AndroidSchedulers.mainThread()) }
fun Completable.io() = compose { observeOn(Schedulers.io()) }
fun Completable.computation() = compose { observeOn(Schedulers.computation()) }

fun <T> Single<T>.mainThread() = compose { observeOn(AndroidSchedulers.mainThread()) }
fun <T> Single<T>.io() = compose { observeOn(Schedulers.io()) }
fun <T> Single<T>.computation() = compose { observeOn(Schedulers.computation()) }


fun <T> Flowable<T>.mainThread() = compose { observeOn(AndroidSchedulers.mainThread()) }
fun <T> Flowable<T>.io() = compose { observeOn(Schedulers.io()) }
fun <T> Flowable<T>.computation() = compose { observeOn(Schedulers.computation()) }

fun Disposable.addToCompositeDisposable(compositeDisposable: CompositeDisposable) {
  compositeDisposable.add(this)
}