package com.instagramtesttask.utils.extensions

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.view.View

/**
 * @author Alex on 20.11.2018.
 */

fun View.show(shouldShow: Boolean = true) {
  this.visibility = if (shouldShow) View.VISIBLE else View.GONE
}

fun View.hide() {
  this.visibility = (View.GONE)
}

fun View.invisible() {
  this.visibility = (View.INVISIBLE)
}

fun View.isVisible(): Boolean {
  return this.visibility == (View.VISIBLE)
}

fun View.isNotVisible(): Boolean {
  return this.visibility != (View.VISIBLE)
}

@JvmOverloads
fun View.showAnim(duration: Long = 200) {
  if (this.isNotVisible()) {
    this.alpha = 0f
    this.visibility = View.VISIBLE
    this.animate()
        .setDuration(duration)
        .alpha(1f)
        .setListener(null)
  }
}

@JvmOverloads
fun View.hideAnim(duration: Long = 200, endListener: (() -> Unit)? = null) {
  if (this.isVisible()) {
    this.animate()
        .setDuration(duration)
        .alpha(0f)
        .setListener(animOnFinish {
          endListener?.invoke()
          this.visibility = View.GONE
        })
  }
}

fun View.invisibleAnim(duration: Long = 200, endListener: (() -> Unit)? = null) {
  if (this.isVisible()) {
    this.animate()
        .setDuration(duration)
        .alpha(0f)
        .setListener(animOnFinish {
          endListener?.invoke()
          this.visibility = View.INVISIBLE
        })
  }
}

fun animOnFinish(callback: () -> Unit): AnimatorListenerAdapter {
  return object : AnimatorListenerAdapter() {
    override fun onAnimationEnd(animation: Animator?) {
      super.onAnimationEnd(animation)
      callback.invoke()
    }
  }
}