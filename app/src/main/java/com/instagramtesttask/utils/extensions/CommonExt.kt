package com.instagramtesttask.utils.extensions

import android.util.Log
import com.instagramtesttask.BuildConfig

/**
 * @author Alex on 20.11.2018.
 */

fun logd(mess: String, tag: String = "myLogs") {
  if(isLogsEnabled())  Log.d(tag, mess)
}

fun isLogsEnabled() = BuildConfig.DEBUG